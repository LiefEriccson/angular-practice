import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  styleUrls: ['./app.component.css'],
  // templateUrl: './app.component.html',
  template: `<h1>{{title}}</h1>
  <h2>{{hero.name}} details!</h2>
  <div><label>id: </label>{{hero.id}}</div>
  <div>
    <label>name: </label>
    <input [(ngModel)]="hero.name" placeholder="name">
</div>`
})
export class AppComponent {
  title = 'Tour of Doges';
  hero: Hero = {
    id: 1,
    name: 'Dogestorm'
  }
}


export class Hero {
  id: number;
  name = 'Dogestorm'
}
